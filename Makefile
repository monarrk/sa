.POSIX:

include config.mk

all: sa-check

sa: sa-check.c
	${CC} -o sa-check sa-check.c

clean:
	rm -f sa-check

run-check: all
	./sa-check bspwm

run: all
	./sa clang

fmt:
	clang-format -i sa-check.c

install: all
	cp sa-check ${PREFIX}/sa-check
	cp sa ${PREFIX}/sa

uninstall:
	rm -f ${PREFIX}/sa-check ${PREFIX}/sa
