// This is free and unencumbered software released into the public domain.
//
// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a compiled
// binary, for any purpose, commercial or non-commercial, and by any
// means.
//
// In jurisdictions that recognize copyright laws, the author or authors
// of this software dedicate any and all copyright interest in the
// software to the public domain. We make this dedication for the benefit
// of the public at large and to the detriment of our heirs and
// successors. We intend this dedication to be an overt act of
// relinquishment in perpetuity of all present and future rights to this
// software under copyright law.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// For more information, please refer to <http://unlicense.org/>⏎

#include <err.h>
#include <errno.h>
#include <fnmatch.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include <fts.h>

#define BUFLEN 100

// read the "cmdline" file from the proc dir. platform specific
void read_cmdline(FTSENT* f, char* target);

void usage(void);
int pmatch(char* dir, const char* pattern, const char* path);
int entcmp(const FTSENT** a, const FTSENT** b);

#define matches(a, b) (strcmp((a), (b)) == 0)
#define fatal(x)                                 \
	{                                            \
		fprintf(stderr, "[SA] Error!: %s\n", x); \
		exit(1);                                 \
	}

#if defined(__linux__)
#define CMDLINE "exe"
#include <unistd.h>
#elif defined(__NetBSD__)
#define CMDLINE "cmdline"
#endif

#if defined(__NetBSD__)
void read_cmdline(FTSENT* f, char* target) {
	FILE* fp = fopen(f->fts_accpath, "r");
	if (!fp)
		fatal("Failed to open file");

	int i = 0;
	char ch;

	char buffer[BUFLEN];

	while ((ch = fgetc(fp)) != EOF) {
		if (ch == ' ') {
			break;
		} else {
			if (i < (BUFLEN - 1)) {
				buffer[i] = ch;
			} else {
				break;
			}
		}

		i += 1;
	}
	buffer[i + 1] = '\0';

	fclose(fp);
	strcpy(target, buffer);
}
#elif defined(__linux__)
void read_cmdline(FTSENT* f, char* target) {
	int size = BUFLEN;
	char buffer[BUFLEN];

	while (1) {
		//buffer = (char*)realloc(buffer, size);
		int nchars = readlink(f->fts_accpath, buffer, size);
		if (nchars < 0) {
			// free(buffer);
			return;
		}

		if (nchars < size)
			return;
		size *= 2;
	}

	strcpy(target, buffer);
}
#endif

// Print the program usage
void usage(void) {
	printf("Usage: sa [PATH]\n");
}

// Compare files by name.
int entcmp(const FTSENT** a, const FTSENT** b) {
	return strcmp((*a)->fts_name, (*b)->fts_name);
}

// Print all files in the directory tree that match the glob pattern.
int pmatch(char* dir, const char* pattern, const char* path) {
	FTS* tree;
	FTSENT* f;
	char* argv[] = { dir, NULL };

	// don't include FTS_LOGICAL because we don't want to follow symlinks
	tree = fts_open(argv, FTS_NOSTAT, entcmp);
	if (tree == NULL)
		err(1, "fts_open");

	int ret = -1;

	// Iterate files in tree. This iteration always skips
	// "." and ".." because we never use FTS_SEEDOT.
	while ((f = fts_read(tree))) {
		switch (f->fts_info) {
		case FTS_DNR: // Cannot read directory
		case FTS_ERR: // Miscellaneous error
		case FTS_NS: // stat() error
		case FTS_DP:
			continue;
		}

		// we found a matching file!
		// check it for the target path
		if (fnmatch(pattern, f->fts_name, FNM_PERIOD) == 0) {
			puts(f->fts_path);

			char buffer[BUFLEN];
			read_cmdline(f, buffer);

			if (matches(path, buffer)) {
				printf("%s", f->fts_parent->fts_name);
				ret = 0;
			}
		}

		// A cycle happens when a symbolic link (or perhaps a
		// hard link) puts a directory inside itself. Tell user
		// when this happens.
		if (f->fts_info == FTS_DC)
			warnx("%s: cycle in directory tree", f->fts_path);
	}

	// fts_read() sets errno = 0 unless it has error.
	if (errno != 0)
		err(1, "fts_read");

	if (fts_close(tree) < 0)
		err(1, "fts_close");

	return ret;
}

int main(int argc, char** argv) {
	if (argc < 2) {
		usage();
		return -1;
	}

	return pmatch("/proc", CMDLINE, argv[1]);
}
