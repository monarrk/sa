# Shell Achievements
achievements for your shell :)

### Building
This project probably has no dependencies, but regardless you'll need:
- [sqlite3](https://sqlite.org/index.html)
- make (tested with BSD make but should be GNU compatible)
- a C compiler \(the default is [clang](https://clang.llvm.org)\)
- the bourne shell \(`/bin/sh`\)
- [mail\(1\)](https://man.netbsd.org/mail.1)

After you've obtained these things, you may wish to change the values in `config.mk` to your liking. View the comments to see the meaning of each value.

Once you are satisfied with the configuration, you'll need to run `make` to build `sa-check` and probably `sudo make install` as well, to install the binary as well as the `sa` script.

### Usage
The first time you run, you'll need to run `sa setup`. This sets up a database in `${HOME}/.config/sa/sa.db`. 

Next, to begin tracking a program, run `sa add PROGRAM`. This will create a row in the database for your process. 

Now when you start up your shell (in your .\*rc or whatever you want) run `sa PROGRAM &` and it will constantly check if you've started running that program. If you have, it'll increment your value in the database.

When you've achieved an achievement, you'll get a message sent to you in the mail with `mail(1)`. You should be able to read this with whatever mail client you use on your local machine.

### Issues
I have written this on and for NetBSD primarily, but I've attempted to make it portable to Linux (Void) as well. That said, the testing over there has been much less rigorous and I expect issues to arrive. This is new software, so if you have trouble, please consider leaving an issue or sending me an [email](mailto:monarrk@aaathats3as.com) so I can fix it for you!

### TODO
- Display achievements on login?
- Improve startup process
- Man page
